<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'wbekq04/TavDxpy7rSPC+ckmvIp8OBWTx+8o/EbmxcTpxfreyr5LJ7FDyevnm/sF29poWT2D29/pXTm+3rElRw==');
define('SECURE_AUTH_KEY',  '7RAQ7q9EIfzzK3duPRaZylNNNpAPB+FbqcOjjyEY3mtkAs8Tx4pJ18n2O+KX6VwAGUNy9K+EDpY3YnGmAoyrCw==');
define('LOGGED_IN_KEY',    'k6snfswzZNToEdqkJzujTi5tbu3aX+oWv/WTP2X+BAJBvRTndcVSQXtrAD3jbuTIZj3qiyFs+tTVL70AfF7YxA==');
define('NONCE_KEY',        'uC5G05ZpTlqp91RyzaxpBnyL4mY0hA9QJPpE9F84SSV/Nb7r2nnIYtxZm52S3V/0OgLp9QlMF/TbbBa0rJvw8A==');
define('AUTH_SALT',        'ZhYveVjZ3e+5izq1uDak1VpRIRZ3vwJde0WzdroczPC1MoWFTMsf+Wd9iy/iJQatKRTJyC1GZhyRr8IuV18r4A==');
define('SECURE_AUTH_SALT', 'aTk+mEgTEYiQT/NYUvwUESm83WOhRcqtJ1rzVVcea0HTZs0EiKidCM9lgi0tSYX0dPEafVKVD7MAdFiBiZRueg==');
define('LOGGED_IN_SALT',   'xGAQEdUHQ9vAf05bBC5E12mJCCaU3cd2xPJIHOkEopoZJaCFcTZpUlvLbf6BrV4AbuqbA/rxYhSbH6xnLjwajg==');
define('NONCE_SALT',       'LWvQgCUovcXZ3KV/Iyl5Tbm1Igf3ADO3UeLe/oiTbAAZI0XpjEwoDmjxmaqQ4uj2TcNx5aF+s8FoqV6Z5jznHQ==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
