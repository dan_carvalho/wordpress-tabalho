<?php   


function crvg_scripts(){
    wp_enqueue_style('reset-style', get_template_directory_uri() . '/reset.css' );
    wp_enqueue_style('normalize-style', get_template_directory_uri() . '/normalize.css' );
    wp_enqueue_style('vasco-style', get_template_directory_uri() . '/style.css' );


}

add_action('wp_enqueue_scripts','crvg_scripts');



function console_log($data) {
    echo '<script>';
    echo 'console.log('.json_encode($data).')';
    echo '</script>';
}


add_theme_support('title-tag');

add_theme_support('menus');

add_theme_support('post-thumbnails', array('noticia'));


function search_filter($query) {
    if ($query->is_search) {
        $query->set('post_type', 'noticia');
    }
    return $query;
} add_filter('pre_get_posts','search_filter');


function custom_post_type_noticias() {
    register_post_type('noticia', array(
        'label'             => 'Notícias',
        'description'       => 'Descrição das notícias',
        'menu-position'     => 2,
        'public'            => true,
        'show_ui'           => true,
        'show_in_menu'      => true,
        'capability_type'   => 'post',
        'map_meta_cap'      => true,
        'hierarchical'      => false,
        'query_var'         => true,
        'supports'          => array('title', 'editor', 'thumbnail'),
        'labels'            => array(
            'name'                => 'Notícia',
            'singular_name'       => 'Notícia',
            'menu_name'           => 'Notícias',
            'add_new'             => 'Nova notícia',
            'add_new_item'        => 'Adicionar nova notícia',
            'edit'                => 'Editar notícia',
            'edit_item'           => 'Editar notícia',
            'new_item'            => 'Nova notícia',
            'view'                => 'Ver notícias',
            'view_item'           => 'Ver notícia',
            'search_items'        => 'Procurar notícia',
            'not_found'           => 'Nennuma notícia encontrada',
            'not_found_in_trash'  => 'Nennuma notícia encontrada na lixeira'
        )
    ));
} add_action('init', 'custom_post_type_noticias');

?>

