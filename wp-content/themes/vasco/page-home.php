<?php 

    /*
        Template Name: Home Page 
    */

?>

<?php get_header() ?>

    <main>
        
        <h1> <?php the_title() ?> </h1>

        <p><?php the_field('paragrafo') ?></p>

        <?php get_template_part('inc', 'section', array('key' => 'texttt')) ?>

    </main>

<?php get_footer() ?> 
