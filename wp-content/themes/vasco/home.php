<?php

/*
  Template Name: Posts
*/
?>


<?php get_header() ?>

     <main>
         <h1>Esse aqui é a Página de Posts</h1>

         <h2>Let's go Posts </h2>

         <p><?php the_field('paragrafo')?></p>

        <?php get_search_form() ?>


        <?php if (have_posts()): while (have_posts()): the_post(); ?>

            <h3> <?php the_title() ?> </h3> 
            <?php the_content() ?>
            <a href="<?php the_permalink()?>">link</a>
        <?php endwhile; else : ?>
            <p><?php esc_html_e('Nenhum post encontrado.'); ?></p>
        <?php endif; echo paginate_links() ?>

        <h2>custom posts</h2>

        <?php 
            $args = array(
                'post_type'         => 'noticia',
                'post_status'       => 'publish',
                'suppress_filters'  => true,
                'orderby'           => 'post_date',
                'order'             => 'DESC'
            );
            $news_query = new WP_Query($args);
        ?>


         <?php get_search_form() ?>

        <?php if ($news_query->have_posts()): while ($news_query->have_posts()): $news_query->the_post(); ?>

            <h3> <?php the_title() ?> </h3> 
            <a href="<?php the_permalink()?>">link</a>
        <?php endwhile; else : ?>
            <p><?php esc_html_e('Nenhum post encontrado.'); ?></p>
        <?php endif; 

            $big = 999999999999;
            echo paginate_links(
                array(
                    'base' => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
                    'format' => '?paged=%#%',
                    'current' => max(1, get_query_var('paged')),
                    'total' => $news_query->max_num_pages
                )
            );
        wp_reset_postdata(); ?>

    </main>

<?php get_footer() ?> 



     </main>

<?php get_footer() ?>